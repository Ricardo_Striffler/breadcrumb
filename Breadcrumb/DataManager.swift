//
//  DataManager.swift
//  Breadcrumb
//
//  Created by Allison Lindner on 15/05/15.
//  Copyright (c) 2015 Ricardo Striffler. All rights reserved.
//

import UIKit
import Parse

class DataManager {
	
	static let sharedInstance = DataManager()
	var sticks: [Stick]
	
	init() {
		sticks = []
	}
	
	func loadDataFromParse() {
		var query = PFQuery(className: Stick.parseClassName())
		query.findObjectsInBackgroundWithBlock { (results, error) -> Void in
			
			if error == nil {
				for result in results! {
					if let stick = result as? Stick {
						self.sticks.append(stick)
					}
				}
			} else {
				println("\(error?.description)")
			}
			
			println("\(self.sticks)")
		}
	}
}
