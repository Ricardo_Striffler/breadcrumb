//
//  ViewController.swift
//  Breadcrumb
//
//  Created by Ricardo Striffler on 14/05/15.
//  Copyright (c) 2015 Ricardo Striffler. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var bgStart: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		DataManager.sharedInstance.loadDataFromParse()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {

        UIView.animateWithDuration(35.0,
            delay: 0.0,
            options: UIViewAnimationOptions.Repeat | UIViewAnimationOptions.Autoreverse | UIViewAnimationOptions.CurveLinear,
            animations: {
                self.bgStart.center.x -= 300
            },completion: nil)
        
    }
}

