//
//  MapController.swift
//  Breadcrumb
//
//  Created by Ricardo Striffler on 14/05/15.
//  Copyright (c) 2015 Ricardo Striffler. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var noteMap: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noteMap.delegate = self
        noteMap.showsUserLocation = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        self.showNotesOnMap()
    }
    
    @IBAction func addNewNote(sender: AnyObject) {
        
        let newNote = UIAlertController(title: "Adicionar Nova Nota", message: "Digite abaixo a nova nota:", preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "Nova Nota", style: .Default) { (_) in
            let NoteTextField = newNote.textFields![0] as! UITextField
            
            self.addNote(NoteTextField.text)
        }
        
        okAction.enabled = false
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        
        newNote.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Nova Nota"
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                okAction.enabled = textField.text != ""
            }
        }
        
        newNote.addAction(okAction)
        newNote.addAction(cancelAction)
        
        presentViewController(newNote, animated: true, completion: nil)
    }
    
    func addNote(wow:String) {
        var nota = wow
        
        var newNote = Stick(className: "Stick")
        newNote.text = nota
        newNote.latitude = self.noteMap.userLocation.coordinate.latitude
        newNote.longitude = self.noteMap.userLocation.coordinate.longitude
        
        newNote.saveInBackgroundWithBlock { (success, error) -> Void in
            var okAction = UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in })
            
            if error == nil {
                if success {
                    var alertController = UIAlertController(title: "Nova Nota", message: "Nota adicionada com sucesso.", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(okAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                    DataManager.sharedInstance.loadDataFromParse()
                    
                    var anotation = MKPointAnnotation()
                    anotation.coordinate.latitude = newNote.latitude
                    anotation.coordinate.longitude = newNote.longitude
                    anotation.title = newNote.text
                    self.noteMap.addAnnotation(anotation)
                } else {
                    var alertController = UIAlertController(title: "Nova Nota", message: "Não foi possivel adicionar a nota.", preferredStyle: .Alert)
                    alertController.addAction(okAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            } else {
                var alertController = UIAlertController(title: "Nova Nota", message: "Erro ao adicionar nota.\nErro: \(error?.description)", preferredStyle: .Alert)
                alertController.addAction(okAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        
        if let location = userLocation {
            noteMap.centerCoordinate = location.location.coordinate
            let regiao = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
            noteMap.setRegion(regiao, animated: true)
            
            for stick in DataManager.sharedInstance.sticks {
                let noteLocation = CLLocation(latitude: stick.latitude, longitude: stick.longitude)
                if location.location.distanceFromLocation(noteLocation) <= 1 {
                    var okAction = UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in })
                    
                    var alertController = UIAlertController(title: "Note", message: "\(stick.text)", preferredStyle: .Alert)
                    alertController.addAction(okAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.noteMap.setRegion(region, animated: true)
    }
    
    func showNotesOnMap() {
        for stick in DataManager.sharedInstance.sticks {
            var anotation = MKPointAnnotation()
            anotation.coordinate.latitude = stick.latitude
            anotation.coordinate.longitude = stick.longitude
            anotation.title = stick.text
            self.noteMap.addAnnotation(anotation)
        }
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        let annotationIdentifier = "noteID"
        var annotationView = noteMap.dequeueReusableAnnotationViewWithIdentifier(annotationIdentifier)
        
        if(annotation.isKindOfClass(MKUserLocation)) {
            return annotationView
        } else if(annotationView == nil) {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            let noteImage = UIImage(named: "note.png")
            annotationView.image = noteImage
            annotationView.canShowCallout = true
        } else {
            annotationView.annotation = annotation
        }
        
        return annotationView
    }

}
