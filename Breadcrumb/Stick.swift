//
//  Stick.swift
//  Breadcrumb
//
//  Created by Allison Lindner on 14/05/15.
//  Copyright (c) 2015 Ricardo Striffler. All rights reserved.
//

import UIKit
import Parse

class Stick: PFObject, PFSubclassing {
	
	@NSManaged var latitude: Double
	@NSManaged var longitude: Double
	@NSManaged var text: String
	
	override class func initialize() {
		struct Static {
			static var onceToken : dispatch_once_t = 0;
		}
		dispatch_once(&Static.onceToken) {
			self.registerSubclass()
		}
	}
	
	static func parseClassName() -> String {
		return "Stick"
	}
}
